## Table of Contents

- [Register A Drone](#register)
- [List of Drones](#droneList)
- [List of Available Drones](#availableDroneList)
- [List of Drone Medications](#droneMedicationList)
- [Get Battery Level of A Drone](#droneBatteryLevel)
- [Load A Drone with Medication](#loadDrone)
- [List of Medications](#medication-list)


<a name="register"></a>
## Register A Drone

The register API will create a drone:

_SpringBoot REST Api / register_
This Endpoint was created by **Abdurami Taibu**

### Request Information

| Type | URL             |
| ---- |-----------------|
| POST | /api/v1/drone   |

### Header

| Type         | Property name    |
| ------------ | ---------------- |
| Allow        | POST, OPTIONS    |
| Content-Type | application/json |
| Vary         | Accept           |

### JSON Body

| Property Name   | type    | required  | Description                                    |
|-----------------|---------| --------- |------------------------------------------------|
| weight          | Integer | true      | Weight of Drone, maximum 500                   |
| serialNumber    | String  | true      | Serial number of Drone (Unique), maxLength 100 characters |

### Error Responses

| Code | Message                                |
| ---- | -------------------------------------- |
| 400  | Provide serial number for the drone with max size of 100 characters             |
| 400  | Entered serial already exists          |
| 400  | Weight of drone should be between 1 and 500       |

### Successful Response Example

```
{
    "data": {
        "id": 1,
        "state": "IDLE",
        "serialNumber": "ZYBTEJWQJFO_0",
        "weight": 150,
        "model": "Middleweight",
        "battery": 100,
        "medications": null,
        "charging": false
    },
    "code": 201,
    "status": "Created Successfully"
}
```


<a name="droneList"></a>

## List of Drones

This service shows the list of drones

_Collection of Your Postman Test / Name of your Postman API Test_
This Endpoint was created by **Abdurami Taibu**

### Header

| Type         | Property name    |
| ------------ | ---------------- |
| Allow        | GET              |
| Content-Type | application/json |

### Request Information

| Type | URL           |
| ---- |---------------|
| GET  | /api/v1/drone |

### Successful Response Example

```
[
    {
        "id": 1,
        "state": "IDLE",
        "serialNumber": "SJLAPJAKJFL_8",
        "weight": 200,
        "model": "Middleweight",
        "battery": 87,
        "medications": [],
        "charging": false
    },
    {
        "id": 2,
        "state": "IDLE",
        "serialNumber": "ABLAPJAKJFL_9",
        "weight": 100,
        "model": "LightWeight",
        "battery": 88,
        "medications": [],
        "charging": false
    }
]
```


<a name="availableDroneList"></a>

## List of Available Drones

This service shows the list of drones available and ready for loading

_Collection of Your Postman Test / Name of your Postman API Test_
This Endpoint was created by **Abdurami Taibu**

### Header

| Type         | Property name    |
| ------------ | ---------------- |
| Allow        | GET              |
| Content-Type | application/json |

### Request Information

| Type | URL                     |
| ---- |-------------------------|
| GET  | /api/v1/drone/available |

### Successful Response Example

```
[
    {
        "id": 1,
        "state": "IDLE",
        "serialNumber": "SJLAPJAKJFL_8",
        "weight": 200,
        "model": "Middleweight",
        "battery": 87,
        "medications": [],
        "charging": false
    },
    {
        "id": 2,
        "state": "LOADING",
        "serialNumber": "ABLAPJAKJFL_9",
        "weight": 100,
        "model": "LightWeight",
        "battery": 88,
        "medications": [],
        "charging": false
    }
]
```


<a name="droneMedicationList"></a>

## List of Drone Medications

This service shows the list of medication on a particular drone

_Collection of Your Postman Test / Name of your Postman API Test_
This Endpoint was created by **Abdurami Taibu**

### Header

| Type         | Property name    |
| ------------ | ---------------- |
| Allow        | GET              |
| Content-Type | application/json |

### Request Information

| Type | URL                                             |
| ---- |-------------------------------------------------|
| GET  | /api/v1/drone/medications/{string:serialNumber} |

### Successful Response Example

```
[
    {
        "id": 1,
        "name": "tramadol",
        "weight": 20,
        "code": "ZYBTEJWQJFO_0",
        "image": "Refund Payment Document.jpeg",
        "imageUri": "/download/wvpVx9kw"
    }
]
```


<a name="droneBatteryLevel"></a>

## Get Battery Level of A Drone

This service shows the battery level of a particular drone

_Collection of Your Postman Test / Name of your Postman API Test_
This Endpoint was created by **Abdurami Taibu**

### Header

| Type         | Property name    |
| ------------ | ---------------- |
| Allow        | GET              |
| Content-Type | application/json |

### Request Information

| Type | URL                                               |
| ---- |---------------------------------------------------|
| GET  | /api/v1/drone/battery-level/{string:serialNumber} |

### Successful Response Example

```
95%
```


<a name="loadDrone"></a>
## Load A Drone with Medication

This service loads a Drone with Medication

_SpringBoot REST Api / load-a-drone_
This Endpoint was created by **Abdurami Taibu**

### Request Information

| Type | URL                |
| ---- |--------------------|
| POST | /api/v1/drone/load |

### Header

| Type         | Property name    |
| ------------ | ---------------- |
| Allow        | POST, OPTIONS    |
| Content-Type | application/json |
| Vary         | Accept           |

### JSON Body

| Property Name | type    | required  | Description                       |
|---------------|---------| --------- |-----------------------------------|
| name          | String  | true      | name of medication                |
| weight        | Integer | true      | Weight of medication, maximum 500 |
| code          | String  | true      | code of medication                |
| image         | Image   | true      | image of medication box           |

### Error Responses

| Code | Message                             |
| ---- | ----------------------------------- |
| 400  | Loading Failed                 |
| 400  | No available Drones for loading     |
| 400  | Medication weight should be less the 500 |
| 400  | Medication should have a valid code |
| 400  | Medication should have a valid name |

### Successful Response Example

```
{
    "data": null,
    "code": 200,
    "status": "Medication loaded successfully on drone ZYBTEJWQJFO_0"
}
```


<a name="medication-list"></a>

## List of Medications

This service shows the list of medications

_Collection of Your Postman Test / Name of your Postman API Test_
This Endpoint was created by **Abdurami Taibu**

### Header

| Type         | Property name    |
| ------------ | ---------------- |
| Allow        | GET              |
| Content-Type | application/json |

### Request Information

| Type | URL                |
| ---- |--------------------|
| GET  | /api/v1/medication |

### Successful Response Example

```
[
    {
        "id": 1,
        "name": "tramadol",
        "weight": 20,
        "code": "ZYBTEJWQJFO_0",
        "image": "Refund Payment Document.jpeg",
        "imageUri": "/download/wvpVx9kw"
    }
]
```
