package com.musala.drone.service;

import com.musala.drone.model.*;
import com.musala.drone.repository.DroneRepository;
import com.musala.drone.repository.MedicationRepository;
import com.musala.drone.utility.ImageUploadUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DroneService {

    private final DroneRepository droneRepository;
    private final MedicationRepository medicationRepository;

    @Autowired
    public DroneService(DroneRepository droneRepository, MedicationRepository medicationRepository){
        this.droneRepository = droneRepository;

        this.medicationRepository = medicationRepository;
    }

    public Response registerDrone(Drone drone) {
        Response response = new Response();

        if(drone.getSerialNumber().isBlank() || drone.getSerialNumber().length() > 100){
            response.setCode(HttpStatus.BAD_REQUEST.value());
            response.setData(null);
            response.setStatus("Provide serial number for the drone with max size of 100 characters");
            return response;
        }

        if(checkUniqueSerialNumber(drone.getSerialNumber())){
            response.setCode(HttpStatus.BAD_REQUEST.value());
            response.setData(null);
            response.setStatus("Entered serial already exists");
            return response;
        }

        if(drone.getWeight() < 0 || drone.getWeight() > 500){
            response.setCode(HttpStatus.BAD_REQUEST.value());
            response.setData(null);
            response.setStatus("Weight of drone should be between 1 and 500");
            return response;
        }

        drone.setBattery(100);
        drone.setState(State.IDLE);
        drone.setCharging(false);

        if(drone.getWeight()  > 0 && drone.getWeight()  <= 100){
            drone.setModel(Model.LightWeight);
        }

        if(drone.getWeight()  > 100 && drone.getWeight()  <= 200){
            drone.setModel(Model.Middleweight);
        }

        if(drone.getWeight()  > 200 && drone.getWeight()  <= 300){
            drone.setModel(Model.Cruiserweight);
        }

        if(drone.getWeight()  > 300){
            drone.setModel(Model.Heavyweight);
        }
        this.droneRepository.save(drone);
        response.setCode(HttpStatus.CREATED.value());
        response.setData(drone);
        response.setStatus("Created Successfully");
        return response;
    }

    public List<Drone> getDrones() { return this.droneRepository.findAll(); }

    public List<Medication> checkDroneMedication(String serialNumber){

        if(serialNumber.length()>0 && serialNumber.length()<=100){
            Optional<Drone> foundDrone = this.droneRepository.findDroneBySerialNumber(serialNumber.trim());
            if(foundDrone.isPresent()){
                return foundDrone.get().getMedications();
            }else{
                throw new IllegalStateException("Drone does not exists");
            }
        }
        throw new IllegalStateException("serial number breaches the size limitations");
    }

    public int checkDroneBatteryLevel(String serialNumber){
        if(serialNumber.length() > 0 && serialNumber.length() <= 100){
            Optional<Drone> foundDrone = this.droneRepository.findDroneBySerialNumber(serialNumber.trim());
            if(foundDrone.isPresent()){
                return foundDrone.get().getBattery();
            }else{
                throw new IllegalStateException("Drone does not exists");
            }
        }
        throw new IllegalStateException("serial number breaches the size limitations");
    }

    public List<Drone> getAvailableDrones(){
        return this.droneRepository.findAll()
                .stream()
                .filter(drone ->
                        (drone.getState().equals(State.IDLE) && checkDroneAvailableWeight(drone)>5) ||
                        (drone.getState().equals(State.LOADING) && checkDroneAvailableWeight(drone)>5)
                )
                .collect(Collectors.toList());
    }

    public Response createMedication(MultipartFile image,
                                     String code,
                                     int weight,
                                     Drone drone,
                                     String name) {
        Response response = new Response();
        if(weight > 499){
            response.setCode(HttpStatus.BAD_REQUEST.value());
            response.setStatus("Medication weight should be less the 500");
            response.setData(null);
            return response;
        }

        if(code.isBlank() || !code.matches("^[A-Z0-9_-]*$")){
            response.setCode(HttpStatus.BAD_REQUEST.value());
            response.setStatus("Medication should have a valid code");
            response.setData(null);
            return response;
        }

        if(name.isBlank() || !name.matches("^[A-Za-z0-9_-]*$")){
            response.setCode(HttpStatus.BAD_REQUEST.value());
            response.setStatus("Medication should have a valid name");
            response.setData(null);
            return response;
        }

        String fileName = StringUtils.cleanPath(Objects.requireNonNull(image.getOriginalFilename()));
        String fileCode;
        try {
            fileCode = ImageUploadUtility.saveFile(fileName, image);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Medication medication = new Medication();
        medication.setCode(code);
        medication.setName(name);
        medication.setWeight(weight);
        medication.setImage(fileName);
        medication.setDrone(drone);
        medication.setImageUri("/download/" + fileCode);
        this.medicationRepository.save(medication);
        response.setCode(HttpStatus.CREATED.value());
        response.setStatus("Created Successfully");
        response.setData(medication);
        return response;
    }
    public Response loadDroneWithMedication(MultipartFile image,
                                            String code,
                                            int weight,
                                            String name) {

       List<Drone> drones = this.droneRepository.findAll()
                .stream()
                .map(drone->{
                    if (this.checkDroneBatteryLevel(drone.getSerialNumber()) < 25){
                        drone.setState(State.LOADED);
                        this.droneRepository.save(drone);
                    }
                    return drone;
                })
                .filter(drone -> drone.getState().equals(State.IDLE) || drone.getState().equals(State.LOADING))
                .collect(Collectors.toList());

       Response response = new Response();

       if(!drones.isEmpty()) {

           for(Drone drone : drones) {
               int availableWeight = checkDroneAvailableWeight(drone);
               if(weight > 0 && weight < 500 && availableWeight > weight) {
                   Response medicationResponse = createMedication(image, code, weight, drone, name);
                   if(medicationResponse.getData()!=null){
                       List<Medication> medics = new ArrayList<>(drone.getMedications());
                       medics.add((Medication) medicationResponse.getData());
                       drone.setMedications(medics);
                       drone.setState(State.LOADING);
                       this.droneRepository.save(drone);
                       response.setCode(HttpStatus.OK.value());
                       response.setStatus("Medication loaded successfully on drone " + drone.getSerialNumber());
                       return response;
                   }
                   return medicationResponse;
               }
           }

       }else {
           response.setCode(HttpStatus.BAD_REQUEST.value());
           response.setStatus("No available Drones for loading");
           return response;
       }

       response.setCode(HttpStatus.BAD_REQUEST.value());
       response.setStatus("Loading Failed");
       return response;
    }

    public void dischargeBattery(){
        List<Drone> drones = this.droneRepository.findAll()
                .stream()
                .filter(drone-> !drone.isCharging())
                .collect(Collectors.toList());

        for(Drone drone : drones){
            if(drone.getBattery() >= 25){
                drone.setBattery(drone.getBattery() - 1 );
                this.droneRepository.save(drone);
            }
            if(drone.getBattery() < 25){
                drone.setCharging(true);
                drone.setState(State.LOADED);
                this.droneRepository.save(drone);
            }
        }
    }

    public void chargeBattery(){
        List<Drone> drones = this.droneRepository.findAll()
                .stream()
                .map(drone -> {
                    if(drone.getBattery() < 10){
                        drone.setCharging(true);
                        this.droneRepository.save(drone);
                        return drone;
                    }
                    return drone;
                })
                .filter(Drone::isCharging)
                .collect(Collectors.toList());

        for(Drone drone : drones){
            if(drone.getBattery()<100){
                drone.setBattery(drone.getBattery() + 1 );
                this.droneRepository.save(drone);
            }
            if(drone.getBattery()==100){
                drone.setCharging(false);
                drone.setState(State.IDLE);
                this.droneRepository.save(drone);
            }
        }
    }

    public boolean checkUniqueSerialNumber(String serialNumber){
        return this.droneRepository.findDroneBySerialNumber(serialNumber).isPresent();
    }

    public int checkDroneAvailableWeight(Drone drone){
        return drone.getWeight() - drone.getMedications()
                .stream().mapToInt(Medication::getWeight).sum();
    }

}
