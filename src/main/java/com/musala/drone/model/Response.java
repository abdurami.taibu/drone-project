package com.musala.drone.model;

public class Response {
    private Object data;
    private String Status;
    private int code;

    public Response() {
    }

    public Response(Object data, String status, int code) {
        this.data = data;
        Status = status;
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}

