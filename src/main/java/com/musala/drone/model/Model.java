package com.musala.drone.model;

public enum Model {
    LightWeight,
    Middleweight,
    Cruiserweight,
    Heavyweight
}
