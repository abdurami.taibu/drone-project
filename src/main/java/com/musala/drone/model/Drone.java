package com.musala.drone.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;

import javax.persistence.*;
import java.util.List;

@Table
@Entity(name="Drone")
public class Drone {
    @Id
    @SequenceGenerator(
            name= "drone_sequence",
            sequenceName = "drone_sequence",
            allocationSize = 1
    )

    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "drone_sequence"
    )
    @Column(
            nullable=false,
            updatable = false
    )
    private Long id;

    private State state;

    @Column(
            nullable=false,
            updatable = false,
            unique = true,
            length = 100
    )

    private String serialNumber;

    private int weight;

    private Model model;

    private boolean isCharging;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private int battery;

    @OneToMany(mappedBy = "drone")
    @JsonManagedReference
    private List<Medication> medications;

    public Drone() {
    }

    public Drone(Long id, State state, String serialNumber, int weight, Model model, boolean isCharging, int battery, List<Medication> medications) {
        this.id = id;
        this.state = state;
        this.serialNumber = serialNumber;
        this.weight = weight;
        this.model = model;
        this.isCharging = isCharging;
        this.battery = battery;
        this.medications = medications;
    }

    public Drone(String serialNumber, int weight) {
        this.serialNumber = serialNumber;
        this.weight = weight;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public int getBattery() {
        return battery;
    }

    public void setBattery(int battery) {
        this.battery = battery;
    }

    public boolean isCharging() {
        return isCharging;
    }

    public void setCharging(boolean charging) {
        isCharging = charging;
    }

    public List<Medication> getMedications() {
        return medications;
    }

    public void setMedications(List<Medication> medications) {
        this.medications = medications;
    }
}
