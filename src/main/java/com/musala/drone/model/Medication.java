package com.musala.drone.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Table
@Entity(name="Medication")
public class Medication {
    @Id
    @SequenceGenerator(
            name= "medication_sequence",
            sequenceName = "medication_sequence",
            allocationSize = 1
    )

    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "medication_sequence"
    )
    @Column(
            nullable=false,
            updatable = false
    )
    private Long id;

    @Column(
            nullable = false,
            updatable = false
    )
    private String name;

    private int weight;

    @Column(
            nullable = false,
            updatable = false
    )
    private String code;

    private String image;

    private String imageUri;

    @ManyToOne
    @JoinColumn(name="drone_id")
    @JsonBackReference
    private Drone drone;

    public Medication() {
    }

    public Medication(Long id, String name, int weight, String code, String image, String imageUri, Drone drone) {
        this.id = id;
        this.name = name;
        this.weight = weight;
        this.code = code;
        this.image = image;
        this.imageUri = imageUri;
        this.drone = drone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public Drone getDrone() {
        return drone;
    }

    public void setDrone(Drone drone) {
        this.drone = drone;
    }

}
