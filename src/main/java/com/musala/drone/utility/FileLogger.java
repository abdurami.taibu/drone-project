package com.musala.drone.utility;

import java.io.*;

public class FileLogger {
    public static void log(String message){
        try{
            BufferedWriter writer = new BufferedWriter(new FileWriter("./log.txt",true));
            writer.write(message + "\n");
            writer.close();
        }catch(IOException ex){
            System.out.println("file not found");
        }
    }
}
