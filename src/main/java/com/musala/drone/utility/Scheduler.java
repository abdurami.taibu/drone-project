package com.musala.drone.utility;

import com.musala.drone.model.Drone;
import com.musala.drone.service.DroneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Component
public class Scheduler {
    private final DroneService droneService;

    @Autowired
    public Scheduler(DroneService droneService) {
        this.droneService = droneService;
    }

    @Scheduled(fixedDelay = 100000)
    public void monitorBatteryLevel() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        List<Drone> drones = this.droneService.getDrones();
        for(Drone drone : drones){
            String message = "Drone " + drone.getSerialNumber() + " has battery= " + drone.getBattery() + "%" + " at "+ strDate;
            FileLogger.log(message);
        }
    }

    @Scheduled(fixedDelay = 30000)
    public void dischargeBatteryJob(){
        this.droneService.dischargeBattery();
    }

    @Scheduled(fixedDelay = 20000)
    public void chargeBatteryJob(){
        this.droneService.chargeBattery();
    }
}
