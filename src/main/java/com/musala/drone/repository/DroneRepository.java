package com.musala.drone.repository;

import com.musala.drone.model.Drone;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DroneRepository extends JpaRepository<Drone,Long> {

    Optional<Drone> findDroneBySerialNumber(String serialNumber);
}
