package com.musala.drone.repository;

import com.musala.drone.model.Drone;
import com.musala.drone.model.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MedicationRepository extends JpaRepository<Medication,Long> {
    Optional<Drone> findMedicationByCode(String code);
}
