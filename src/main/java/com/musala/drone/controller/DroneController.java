package com.musala.drone.controller;

import com.musala.drone.model.Drone;
import com.musala.drone.model.Medication;
import com.musala.drone.model.Response;
import com.musala.drone.service.DroneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping(path="/api/v1")
public class DroneController {

    private final DroneService droneService;

    @Autowired
    public DroneController(DroneService droneService){
        this.droneService = droneService;
    }

    @PostMapping ("/drone" )
    public Response registerDrone (@RequestBody Drone drone){
       return this.droneService.registerDrone(drone);
    }

    @GetMapping("/drone")
    public List<Drone> getDrones(){
        return this.droneService.getDrones();
    }

    @GetMapping("/drone/medications/{serialNumber}")
    public List<Medication> checkDroneMedication(@PathVariable("serialNumber") String serialNumber){
        return this.droneService.checkDroneMedication(serialNumber);
    }

    @GetMapping("/drone/battery-level/{serialNumber}")
    public String checkDroneBatteryLevel(@PathVariable("serialNumber") String serialNumber){
        String result;
        try{
           result = this.droneService.checkDroneBatteryLevel(serialNumber) + "%";
        }catch(IllegalStateException e){
            throw new IllegalStateException("Drone does not exists");
        }
        return result;
    }

    @GetMapping("/drone/available")
    public List<Drone> getAvailableDrones(){
        return this.droneService.getAvailableDrones();
    }

    @PostMapping("/drone/load")
    public Response loadDroneWithMedication(@RequestParam("image") MultipartFile image,
                                            @RequestParam("code") String code,
                                            @RequestParam("weight") int weight,
                                            @RequestParam("name") String name){

        return this.droneService.loadDroneWithMedication(image, code, weight, name);
    }
}
