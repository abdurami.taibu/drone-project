package com.musala.drone.controller;


import com.musala.drone.model.Medication;
import com.musala.drone.service.MedicationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController

public class MedicationController {

    private final MedicationService medicationService;

    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping(path = "api/v1/medication")
    public List<Medication> getMedications() {
        return this.medicationService.getMedications();
    }

    @GetMapping("/download/{fileCode}")
    public ResponseEntity<?> downloadFile(@PathVariable("fileCode") String fileCode) {
        return this.medicationService.downloadFile(fileCode);
    }
}
