package com.musala.drone;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.musala.drone.controller.DroneController;
import com.musala.drone.model.*;
import com.musala.drone.repository.DroneRepository;
import com.musala.drone.service.DroneService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(DroneController.class)
class DroneApplicationTests {

    @Autowired
    MockMvc mockMvc;
    @Autowired
    ObjectMapper mapper;

    @MockBean
    DroneRepository droneRepository;

    @MockBean
    DroneService droneService;

    Drone RECORD_1 = new Drone(
            1L,
            State.IDLE,
            "HEYLN_4AS",
            100,
            Model.LightWeight,
            false,
            100,
            List.of()
    );

    Drone RECORD_2 = new Drone(
            2L,
            State.LOADING,
            "AGVWI9O_2",
            200,
            Model.Middleweight,
            false,
            100,
            List.of()
    );

    Medication medication = new Medication(
            1L,
            "paracetamol",
            20,
            "skUYLL",
            "paracetamol.jpg",
            "download/YIN_4",
            null
    );
    Drone RECORD_3 = new Drone(
            3L,
            State.LOADED,
            "YUMWEI_",
            300,
            Model.Cruiserweight,
            false,
            100,
            List.of(medication)
    );

    @Test
    public void getDrones() throws Exception {
        List<Drone> drones = new ArrayList<>(Arrays.asList(RECORD_1, RECORD_2));

        Mockito.when(droneService.getAvailableDrones()).thenReturn(drones);

        mockMvc.perform(get("/api/v1/drone/available")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[1].serialNumber", is("AGVWI9O_2")));
    }


    @Test
    public void getAvailableDrones() throws Exception {
        List<Drone> drones = new ArrayList<>(Arrays.asList(RECORD_1, RECORD_2, RECORD_3));

        Mockito.when(droneService.getDrones()).thenReturn(drones);

        mockMvc.perform(get("/api/v1/drone")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].serialNumber", is("HEYLN_4AS")));
    }

    @Test
    public void checkDroneBatteryLevel() throws Exception {
        Mockito.when(droneService.checkDroneBatteryLevel("AGVWI9O_2")).thenReturn(100);

        mockMvc.perform(get("/api/v1/drone/battery-level/AGVWI9O_2")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()));
    }

    @Test
    public void checkDroneMedications() throws Exception {
        List<Medication> medications = List.of(medication);

        Mockito.when(droneService.checkDroneMedication("YUMWEI_")).thenReturn(medications);

        mockMvc.perform(get("/api/v1/drone/medications/YUMWEI_")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));
    }

    @Test
    public void registerDrone() throws Exception {
        Drone drone = new Drone(
                1L,
                State.IDLE,
                "YUMWEI4_",
                300,
                Model.Cruiserweight,
                false,
                100,
                List.of()
        );

        Drone drone1 = new Drone("YUMWEI4_", 300);

        Response response = new Response(drone, "Created Successfully", HttpStatus.CREATED.value());

        Mockito.when(droneService.registerDrone(Mockito.any(Drone.class))).thenReturn(response);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/api/v1/drone")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.ALL_VALUE)
                .content(mapper.writeValueAsString(drone1));

        MvcResult result = mockMvc.perform(mockRequest).andReturn();

        MockHttpServletResponse resultResponse = result.getResponse();

        assertEquals(HttpStatus.OK.value(), resultResponse.getStatus());
        assertEquals("{" +
                        "\"data\":{" +
                        "\"id\":1," +
                        "\"state\":\"IDLE\"," +
                        "\"serialNumber\":\"YUMWEI4_\"," +
                        "\"weight\":300," +
                        "\"model\":\"Cruiserweight\"," +
                        "\"battery\":100," +
                        "\"medications\":[]," +
                        "\"charging\":false}," +
                        "\"code\":201," +
                        "\"status\":\"Created Successfully\"" +
                        "}",
                resultResponse.getContentAsString()
        );
    }
}
